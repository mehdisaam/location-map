import { useEffect,useRef,useState } from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import { useSelector, useDispatch } from 'react-redux'
import MapMain from './Components/Map'
import EditeLocation from './Components/Map/edite-popup'
function App() {
  
  return (
    <div className="App">
    
    <MapMain/>
    </div>
  )
}
export default App
