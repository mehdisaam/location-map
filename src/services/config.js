export default[
   {
      "id": 337,
      "locationId": "londonwestfield",
      "name": "London - Westfield, UK",
      "status": "OPEN",
      "address": {
         "street": "Westfield Shopping Town",
         "city": "London",
         "state": null,
         "zip": "W12 7GD",
         "countryId": 108,
         "country": "United Kingdom",
         "regionId": 101,
         "region": "Europe"
      },
      "gps": {
         "latitude": 51.507888,
         "longitude": -0.221972
      },
      "dateOpened": "2014-10-16",
      "stallCount": 24,
      "counted": true,
      "elevationMeters": 9,
      "powerKilowatt": 150,
      "solarCanopy": false,
      "battery": false,
      "statusDays": 0,
      "urlDiscuss": true
   },
{
      "id": 379,
      "locationId": "londontowersupercharger",
      "name": "London Tower, UK",
      "status": "CLOSED_PERM",
      "address": {
         "street": "St. Katharines Way",
         "city": "London",
         "state": null,
         "zip": "E1W 1LD",
         "countryId": 108,
         "country": "United Kingdom",
         "regionId": 101,
         "region": "Europe"
      },
      "gps": {
         "latitude": 51.506333,
         "longitude": -0.072778
      },
      "stallCount": 2,
      "counted": true,
      "elevationMeters": 12,
      "powerKilowatt": 130,
      "solarCanopy": false,
      "battery": false,
      "statusDays": 195,
      "urlDiscuss": true
   },


 {
      "id": 511,
      "locationId": "brentcross",
      "name": "London - Brent Cross, UK",
      "status": "OPEN",
      "address": {
         "street": "Brent Cross, Prince Charles Drive",
         "city": "London",
         "state": null,
         "zip": "NW4 3FP",
         "countryId": 108,
         "country": "United Kingdom",
         "regionId": 101,
         "region": "Europe"
      },
      "gps": {
         "latitude": 51.57753,
         "longitude": -0.223753
      },
      "dateOpened": "2015-01-03",
      "stallCount": 2,
      "counted": true,
      "elevationMeters": 53,
      "powerKilowatt": 130,
      "solarCanopy": false,
      "battery": false,
      "statusDays": 0,
      "urlDiscuss": true
   },
]