import { useState, useEffect } from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import * as L from 'leaflet'
import imgUpload from '../../assets/image/icon-upload.png'
const EditeLocation = (props, iconImg) => {
  const [myimage, setMyImage] = useState('2')

  const uploadImage = (e) => {
    setMyImage(URL.createObjectURL(e.target.files[0]))
  }
  var getUrlImg = new L.icon({
    iconUrl: require(`../../assets/image/${myimage}.png`),
    iconSize: [40],
  })

  console.log(myimage, 'myimage')
  const [map, setMap] = useState(null)
  const position = [51.505, -0.09]
  return (
    <>
      <div className="popup-main">
        <div className="popup-header"></div>
        <div className="popup-content">
          <div className="location-part">
            <label> Location Name : </label>
            <input value={props.tesla.locationId} />
          </div>
          <div className="map-inside">
            <MapContainer
              whenCreated={setMap}
              center={position}
              zoom={13}
              scrollWheelZoom={false}
            >
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker
                position={[props.tesla.gps.latitude, props.tesla.gps.longitude]}
                // icon={getUrlImg}
              ></Marker>
            </MapContainer>
          </div>
          <div className="upload-img">
          <label for="file-input">
            <div class="upload-box-img">
              <img src={imgUpload}/>
            </div>
            </label>
            <input id="file-input" type="file" onChange={uploadImage} />
          </div>
          <div class="btn-group-main">
          <button type='button' onClick={props.click}>Save</button>
          <button type='button' onClick={props.click}>Cancel</button>
        </div>
        </div>
      
      </div>
    </>
  )
}

export default EditeLocation
