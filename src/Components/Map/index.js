import { useEffect,useRef,useState } from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import { useSelector, useDispatch } from 'react-redux'
import EditeLocation from '../../Components/Map/edite-popup'

const MapMain = ()=>{

  const [map, setMap] = useState(null);
  const [editePage,setEditePage] = useState(false)
 const mapRef = useRef(null)
 const closeMapPopup = ()=>{
   if (!mapRef.current || !map)
  return mapRef.current._close();
 }
 const openEdite = ()=>{
   setEditePage(!editePage)
 }
 const position = [51.505, -0.09]
  const todos = useSelector((state) => state.data.datas)
  return (
    <div className="App">
      <MapContainer     whenCreated={setMap}  center={position} zoom={13} scrollWheelZoom={false}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {todos?.map((tesla) => (
          <Marker  position={[tesla.gps.latitude, tesla.gps.longitude]}>
            <Popup ref={mapRef} closeButton={true}>
           
            {!editePage ?  <div className='edite-popup'> <div className="popup-header">{tesla.name}</div>
              <div className='content-popup'>
                <h4>State : <span>{tesla.status}</span></h4>
                <h5>Street Name : <span>{tesla.address.street}</span></h5>
              </div>
              <div className='btn-popup-group'>
                <button type='button' onClick={openEdite}>Edite</button>
                <button onClick={closeMapPopup}>close</button>
              </div>
                </div>
            : <EditeLocation tesla={tesla} click={openEdite}/>
            }
           
          
            
            
           
          
            </Popup>
          </Marker>
        ))}
      </MapContainer>
    
    </div>
  )
}

export default MapMain