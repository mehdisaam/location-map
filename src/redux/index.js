import { configureStore } from "@reduxjs/toolkit";
import dataSlice from './redux-tooltik'

const store = configureStore({
    reducer:{
        data:dataSlice,
    }
})
export default store